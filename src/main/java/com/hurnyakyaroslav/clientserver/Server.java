package com.hurnyakyaroslav.clientserver;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;


public class Server {

    private int port = 8090;
    private final ByteBuffer buffer = ByteBuffer.allocate(256);

    static public void main(String args[]) throws Exception {
        new Server().startServer();
    }

    public void startServer() {
        try (ServerSocketChannel ssc = ServerSocketChannel.open()) {
            System.out.println(ssc.getLocalAddress());
            // Adjusts this channel's blocking mode.
            ssc.configureBlocking(false);

            ServerSocket ss = ssc.socket();

            // ServerSocketChannel: selectable channel for stream-oriented listening sockets
            InetSocketAddress isa = new InetSocketAddress(port);
            // Binds the channel's socket to a local address and configures the socket to listen for connections
            ssc.bind(isa);
            // Selector: multiplexor of SelectableChannel objects
            Selector selector = Selector.open(); // selector is open here
            ssc.register(selector, SelectionKey.OP_ACCEPT);
            System.out.println("Listening on port: " + port);

            System.out.println(isa);

            // Infinite loop..
            // Keep server running
            while (true) {

                // log("i'm a server and i'm waiting for new connection and buffer select...");
                // Selects a set of keys whose corresponding channels are ready for I/O operations
                int num = selector.select();

                if (num == 0){
                    continue;
                }

                // token representing the registration of a SelectableChannel with a Selector
                Set<SelectionKey> keys = selector.selectedKeys();
                Iterator<SelectionKey> it = keys.iterator();

                while (it.hasNext()) {
                    SelectionKey key = it.next();
                    //System.out.println(key);
                    if ((key.readyOps() & SelectionKey.OP_ACCEPT) == SelectionKey.OP_ACCEPT) {
                        System.out.println("acc");
                        Socket s = ss.accept();
                        System.out.println("Got connection from " + s);

                        SocketChannel sc = s.getChannel();
                        sc.configureBlocking(false);
                        sc.register(selector, SelectionKey.OP_READ);

                    } else if ((key.readyOps() & SelectionKey.OP_READ) == SelectionKey.OP_READ) {
                        SocketChannel sc = null;
                        try {
                            sc = (SocketChannel) key.channel();
                            boolean ok = processInput(sc);
                            if (!ok) {
                                key.cancel();
                                Socket s = null;
                                try {
                                    s = sc.socket();
                                    s.close();
                                } catch (IOException e) {
                                    System.out.println("Error closing socket: " + s + ":" + e);
                                }
                            }
                        } catch (IOException e) {
                            key.cancel();
                            try {
                                sc.close();
                            } catch (IOException ea) {
                                System.out.println(ea);
                            }
                            System.out.println("Closed: " + sc);
                        }
                    }

                }
                keys.clear();
            }
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    private boolean processInput(SocketChannel sc) throws IOException {
        buffer.clear();
        sc.read(buffer);
        buffer.flip();
        if (buffer.limit() == 0) {
            return false;
        }
        for (int i = 0; i > buffer.limit(); i++) {
            byte b = buffer.get(i);
            buffer.put(i, b);
        }
        sc.write(buffer);
        System.out.println("Processed " + buffer.limit() + " from " + sc);
        return true;
    }
}

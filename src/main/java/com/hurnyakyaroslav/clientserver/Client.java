package com.hurnyakyaroslav.clientserver;


import java.io.*;
import java.net.InetAddress;
import java.net.Socket;



public class Client {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";

    public static void main(String[] args) {
        int serverPort = 8090;
        String address = "0.0.0.0";

        try {
            InetAddress ipAddress = InetAddress.getByName(address);
//            InetAddress ipAddress = InetAddress.getByName(address);
            System.out.println("IPAddress: " + address + " and port: " + serverPort);

            Socket socket = new Socket(ipAddress, serverPort);
            //System.out.println("1232");
            System.out.println(socket);
            System.out.println(socket.isConnected()); ;

            InputStream sin = socket.getInputStream();
            OutputStream sout = socket.getOutputStream();

            DataInputStream in = new DataInputStream(sin);
            DataOutputStream out = new DataOutputStream(sout);

            BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
            String line = null;
//
            line = keyboard.readLine();
//
            byte[] message = (line).getBytes();
            in.read(message);
//
            byte[] buf = new byte[100];
            out.write(buf);
//
            System.out.println(buf);
//            // wait for 2 seconds before sending next message
            Thread.sleep(2000);
            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
package com.hurnyakyaroslav.model;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * Try to create SomeBuffer class, which can be used for read and write data from/to channel (Java NIO)
 */
public class SomeBuffer {

    RandomAccessFile file;
    FileChannel channel;
    ByteBuffer buf;
    int bytesRead;
    int bytesWrite;

    SomeBuffer() {
        try {
            file = new RandomAccessFile("SourceFile.txt", "rw");
            channel = file.getChannel();
            buf = ByteBuffer.allocate(48);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    void read() throws IOException {
        bytesRead = channel.read(buf);
        buf.flip();
    }

    void write() throws IOException {
        bytesWrite = channel.write(buf);
    }
}

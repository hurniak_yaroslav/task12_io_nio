package com.hurnyakyaroslav.model;

import com.hurnyakyaroslav.model.droids.Ship;

import java.io.*;
import java.util.Arrays;
import java.util.Scanner;

public class Model {

    //*********
    public String testSerialization() {
        String result = new String("");
        Ship droidShip = new Ship("first", "second", "third", "fourth");
        result += "Початкові дані" + droidShip.toString();
        File file = new File("savedDroids.bin");
        saveDroids(droidShip, file);
        result += "\n Зчитані дані: ";
        result += readDroids(file);
        return result;
    }

    private void saveDroids(Ship droidShip, File file) {
        try (ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(file))) {
            os.writeObject(droidShip);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            System.out.println("Ship had been written.");
        }

    }

    private String readDroids(File file) {
        Ship droidShip = null;
        try (ObjectInputStream is = new ObjectInputStream(new FileInputStream(file))) {
            droidShip = (Ship) is.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            System.out.println("Ship had been readden.");
        }
        return droidShip.toString();
    }

    //***********

    public String testReaders() {
        String result = new String();
        long startTime = System.nanoTime();
        result += "\n" + testUsualReader() + "\n";
        long endTime = System.nanoTime();
        result += "time: " + ((endTime - startTime) / 1000) + "\n";

        for (int i = 5; i < 100; i += 10) {
            result += runBufferedReader(i);
        }

        return result;
    }

    private String runBufferedReader(int bufferSize) {
        String result = new String();
        long startTime = System.nanoTime();
        result += testBufferedReader(bufferSize);
        long endTime = System.nanoTime();
        return result += " time: " + ((endTime - startTime) / 1000) + "\n";
    }

    private String testUsualReader() {
        int count = 0;
        try (InputStream is = new FileInputStream(new File("Thinking_in_Java.pdf"))) {
            int data = is.read();
            count = 1;
            while (data != -1) {
                data = is.read();
                count++;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "Size: " + count;
    }

    private String testBufferedReader(int bufferSize) {
        int count = 0;
        try (BufferedInputStream is = new BufferedInputStream(new FileInputStream(new File("Thinking_in_Java.pdf")), bufferSize)) {

            int data = is.read();
            count = 1;
            while (data != -1) {
                data = is.read();
                count++;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return " Buffer: " + bufferSize + " Size: " + count;
    }
    //****************

    /**
     * Write a program that reads a Java source-code file (you provide the file name on the command line)
     * and displays all the comments. Do not use regular expression.
     *
     * @param filename name of file for find process
     * @return String of comments
     */
    public String findComments(String filename) {
        File file = new File(filename);
        String comments = " ";
        try (BufferedInputStream is = new BufferedInputStream(new FileInputStream(file))) {
            int data = 1;
            while (data != -1) {
                data = is.read();
                if (data == '"') {
                    data = is.read();
                    while (data != '"') {
                        data = is.read();
                    }
                }
                if (data == '/') {
                    data = is.read();
                    if (data == '/') {
                        comments += commentProcessing(is, '\n');
                    } else if (data == '*') {
                        comments += commentProcessing(is, '*', '/');
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return comments;
    }

    @Deprecated
    private String lineCommentProcessing(BufferedInputStream is) {
        String comment = "";
        int data = 1;
        do {
            comment += (char) data;
            try {
                data = is.read();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } while (data != '\n');
        return comment + "\n";
    }

    private boolean checkComparing(BufferedInputStream is, int data, char... args) throws IOException {
        for (char s : args) {
            if (data != s) return false;
            data = is.read();
        }
        return true;
    }

    private String commentProcessing(BufferedInputStream is, char... chars) {
        String comment = "";
        int data = 1;
        try {
            do {
                comment += (char) data;
                data = is.read();
                if (data == chars[0]) {
                    if (checkComparing(is, data, chars)) {
                        break;
                    }
                }
            } while (true);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return comment + "\n";
    }
    //**********************

    /**
     * Write a program that displays the contents of a specific directory (file and
     * folder names + their attributes) with the possibility of setting the current
     * directory (similar to “dir” and “cd” command line commands).
     */
    public void displayDirectoryContent(File directory) {
    if(directory.exists()){
        printDirectory(directory, " ");
    }
    }

    private void printDirectory(File directory, String separator){
        File[] files = directory.listFiles();
        Arrays.stream(files).forEach(file -> {
            if(file.isDirectory()){
                System.out.println(separator+"Directory "+ file.getName());
                printDirectory(file, separator+"\t");
            }
            else {
                System.out.println(separator+"File: " + file.getName());
            }
        });
    }

    //Test Channel
    public void testChannel(){
        SomeBuffer sb = new SomeBuffer();

        try {
            sb.read();
            sb.write();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }





}

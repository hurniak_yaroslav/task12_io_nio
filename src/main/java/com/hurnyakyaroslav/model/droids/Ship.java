package com.hurnyakyaroslav.model.droids;

import java.io.Serializable;
import java.util.ArrayList;

public class Ship implements Serializable {

    private ArrayList<Droid> droids;

    Ship() {
        droids = new ArrayList<>();
    }

    public Ship(String... args) {
        droids = new ArrayList<>();
        for (String s : args) {
            droids.add(new Droid(s, (int) (Math.random() * 100)));
        }
    }

    public ArrayList<Droid> getDroids() {
        return droids;
    }

    @Override
    public String toString() {
        return droids.toString();
    }
}

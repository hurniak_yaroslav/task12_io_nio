package com.hurnyakyaroslav.controller;

import com.hurnyakyaroslav.model.Model;

import java.io.*;

public class Controller {
    Model model = new Model();

    /**
     * Create Ship with Droids. Serialize and deserialize them. Use transient.
     *
     * @return String info
     */
    public String readWriteShip() {
        return model.testSerialization();
    }

    public String testReaders(){
        return model.testReaders();
    }

    public String findComments(String filename){
        return model.findComments(filename);
    }

    public String displayDirectoryContent(File directory){
        model.displayDirectoryContent(directory);
        return null;
    }

    public void testChannel(){
        model.testChannel();
    }


}

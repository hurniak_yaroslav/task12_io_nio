package com.hurnyakyaroslav.view;

import com.hurnyakyaroslav.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ConsoleView {
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input;
    private static Logger logger = LogManager.getLogger(ConsoleView.class);

    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - TestSerialization");
        menu.put("2", "2 - Compare usual and buffered reader ");
        menu.put("3", "3 - Print comments in java source file ");
        menu.put("4", "4 - Display directory content ");
        menu.put("5", "5 - Test NIO channel");
        menu.put("6", "6 - ");
        menu.put("Q", "Q");
    }

    public ConsoleView() {

        controller = new Controller();
        input = new Scanner(System.in);

        setMenu();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::testSerialization);
        methodsMenu.put("2", this::compareReaders);
        methodsMenu.put("3", this::getComments);
        methodsMenu.put("4", this::displayDirectoryContent);
        methodsMenu.put("5", this::testNIOChannel);
        methodsMenu.put("6", this::testNIOChannel);
    }

    private void testSerialization() {
        logger.info(controller.readWriteShip());
    }

    private void compareReaders() {
        logger.info(controller.testReaders());
    }

    private void getComments() {
        logger.info("Enter filename for processing: ");
        Scanner scanner = new Scanner(System.in);
        String filename = scanner.nextLine();
        logger.info(controller.findComments(filename));
    }

    private void displayDirectoryContent() {
        Scanner scanner = new Scanner(System.in);
        logger.info("Enter full path to directory: ");
        String dirPath = scanner.nextLine();
        dirPath = "/Users/Admin/NetBeansProjects";
        File directory = new File(dirPath);
        controller.displayDirectoryContent(directory);
        String command;
        do {
            command = scanner.nextLine();
            if (command.equals("Q")) return;

            Pattern pattern = Pattern.compile("(?<commandValue>cd|ls) (?<dirName>.*)$");
            Matcher matcher = pattern.matcher(command);
            if (matcher.find()) {
                System.out.println("Command: " + matcher.group("commandValue"));
                System.out.println("Directory`s name: " + matcher.group("dirName"));
                String newDirPath = dirPath + "/" + matcher.group("dirName");
                File newFile = new File(newDirPath);
                if (newFile.isDirectory()) {
                    controller.displayDirectoryContent(newFile);
                    if (matcher.group("commandValue").equals("cd")) {
                        dirPath = newDirPath;
                    }
                } else {
                    logger.warn("File isn't directory! Try again.");
                }
            } else {
                logger.fatal("Wrong command.");
            }
        }
        while (true);
    }

    private void testNIOChannel() {
        controller.testChannel();
    }

    //-------------------------------------------------------------------------
    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }
}
